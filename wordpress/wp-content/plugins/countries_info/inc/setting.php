<?php

	function wpac_settings_page_html() {

		if (!is_admin()){
			return;
		}
		?>

			<div class="wrap">
				<form action="option.php" method="POST">
					<?php 
						settings_fields ('wpac-settings');
						do_settings_sections('wpac-settings');
						
						submit_button('Save Changes');
					?>
				</form>
			</div>

		<?php


	}


	// Top level administration Menu
	function wpac_register_menu_page(){

		add_menu_page('WPAC LIKE SYSTEM', 'Capitals', 'manage_options', 'Capitals', 'wpac_settings_page_html', 'dashicons-thumbs-up', '30');

	}
	// call function  = - > wpac_register_menu_page
	add_action('admin_menu', 'wpac_register_menu_page');


	function wpac_plugin_settings(){
		register_setting('wpac-settings', 'wpac_add_btn_countries');
		register_setting('wpac-settings', 'wpac_add_btn_capitals');

		add_settings_section('wpac_label_settings_section', 'Add Country And Capital', 'wpac_plugin_settings_section_cd', 'wpac-settings');

		add_settings_field('wpac_add_countries_btn_settings', 'Add Country', 'wpac_add_btn_field_cb', 'wpac-settings', 'wpac_label_settings_section');
		
		add_settings_field('wpac_add_capital_btn_settings', 'Add Capital', 'wpac_add_btn_field_cb', 'wpac-settings', 'wpac_label_settings_section');
	}	

	add_action('admin_init', 'wpac_plugin_settings');


	// call back function for section
	function wpac_plugin_settings_section_cd(){
		echo "<p>Button Labels</p>";
	}

	// call back function for Country
	function wpac_add_btn_field_cb(){
		$setting = get_option('wpac_add_btn_countries');
	?>
		<input type="text" name="wpac_add_btn_countries" value="<?php echo isset($setting)? esc_attr($setting): " " ?>">
		<?php	
	}

	// call back function for capital
	function wpac_add_capital_btn_field_cb(){
		$setting = get_option('wpac_add_btn_capitals');
	?>
		<input type="text" name="wpac_add_btn_capitals" value="<?php echo isset($setting)? esc_attr($setting): " " ?>">
		<?php	
	}
	?>