<?php

/*

	Plugin Name: Capitals
	Plugin URI: https://capital.com/
	Description: This is testing capitals of countries
	Author: Admin
	Author URI: https:/capital.com/
	Version: 5.3.
*/

// if this is called directly, abort
if (!defined('WPINC')){
	die(); 
}

// check version
if (!defined('WPAC_PLUGIN_VERSION')){
	define('WPAC_PLUGIN_VERSION', '1.0.0');
}

// check or define dir of wordpress
if (!defined('WPAC_PLUGIN_DIR')){
	define('WPAC_PLUGIN_DIR', plugin_dir_url(__FILE__));
}

//check this constant before define or exists
if ( !function_exists('wpac_plugin_scripts')){
	function wpac_plugin_scripts(){
		wp_enqueue_style('wpec-js', WPAC_PLUGIN_DIR.'assets/css/style.css');
		wp_enqueue_script('wpec-js', WPAC_PLUGIN_DIR.'assets/js/main.js');

	}
	add_action('wpac_enqueue_scripts', 'wpac_plugin_scripts', 'jQuery', '1.0.0', true);
} 

// Setting page html

require plugin_dir_path(__FILE__).'inc/settings.php';


function wpac(){
	global $wpdb;
	
	$table_name = $wpdb->prefix . "countries_info"; 
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
	  id mediumint(9) NOT NULL AUTO_INCREMENT,
	  time timestamp DEFAULT '0000-00-00 00:00:00' NOT NULL,
	  user_id mediumint(9) NOT NULL,
	  country varchar(55) DEFAULT '' NOT NULL,
	  capital varchar(55) DEFAULT '' NOT NULL,
	  PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
			
	}
	

?>



