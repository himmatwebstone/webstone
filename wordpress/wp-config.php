<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('FS_METHOD', 'direct');

define( 'DB_NAME', 'country_info' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@rGI>Dl)w(F6)l(Z4[xS~*ard=+=[j+SNudc=3#:v%SP5;%>&yg/o_^`g#_m->w`' );
define( 'SECURE_AUTH_KEY',  ')pIAF`p$arW5e$6~#]M3DP;>E)QX$Y.^kUt]Og:/f#lQ7z~M;_ML;ww]{5>-v:,|' );
define( 'LOGGED_IN_KEY',    'iMeQ(4P|~2y[pO+#=)k]o#;dFQ!Jp&-VV;k,g37?*fvlelc6>+U-Lrkj!wrAcpWY' );
define( 'NONCE_KEY',        '^$NQfomTqU F8yYBF9(v_7TZH-%5|}$Xw`Iwmk@ai?55|hdC9E691@z/;:K2mZ-a' );
define( 'AUTH_SALT',        'D.9Im MtM:UcyJ=&_|8WaJs0vf56NJz@r#ds=QY9RN9](/+1CnbPDC6Dyxv3*Bm+' );
define( 'SECURE_AUTH_SALT', '$BIh_wh}xJ89#g,q^k/.id!E#cPL4.5$*cz.B?7<|@%|YvUD>_7hpcnL]:w&$Kvn' );
define( 'LOGGED_IN_SALT',   ':~.RZvlq4,kklh9r=cSsl>yg}5:(-X0vQ-a`?T/I>})Bfm+(iBD U5kLGnFWYw{i' );
define( 'NONCE_SALT',       'pgtT_6k*iKW`$`7L$/7.dW3&byr0KC>yrI>e97IXiX|%l).O`,N#`z$bsuT&(IuH' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
